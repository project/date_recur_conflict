TABLE OF CONTENTS
-----------------

 * Introduction
 * Requirements
 * Installation
 * Basic usage
 * Alter queries (for developers)
 * Issues


INTRODUCTION
------------

This module provides validation to Date Recur fields in order to prevent
overlapping dates from being saved.


REQUIREMENTS
------------

Only the Date Recur module (https://drupal.org/project/date_recur) is required.

Please refer to its documentation for instructions on how to install and
configure it.


INSTALLATION
------------

With the required modules added to your Drupal installation, just add the 
Date Recur Conflict's folder under the Drupal's modules folder and enable it in
the "admin/modules" page as usual.


CONFIGURATION
-------------

Besides enabling validation for specific fields, there are no settings for this
module.


BASIC USAGE 
-----------

The validation must be enabled on a field instance basis to take effect.

Steps to enable the validation for a field:

 1. Go to the Manage Fields page for your entity bundle (or content type).
 2. Click 'Edit' to go to the settings page of a date_recur field.
 3. In the settings form of date_recur fields, a "Prevent overlapping 
    occurrences" checkbox will be shown. Mark it to enable the validation for
    this field.
 4. Save the settings (submit the form).


ALTER QUERIES
-------------

The database queries used to check overlapping occurrences can be altered by
the usual Drupal hooks. These queries are given the following tags:

 * date_recur_conflict__{ENTITY_TYPE}__{BUNDLE}__{FIELD_NAME}
 * date_recur_conflict__{ENTITY_TYPE}__{BUNDLE}
 * date_recur_conflict__{ENTITY_TYPE}__{FIELD_NAME}
 * date_recur_conflict__{ENTITY_TYPE}
 * date_recur_conflict__{FIELD_NAME}

With {ENTITY_TYPE}, {BUNDLE}, and {FIELD_NAME} replaced by the entity type ID,
the bundle name, and the field name, respectively.

The following metadata will be available to the hook implementation:

 * occurrence: A \Drupal\date_recur\DateRange object being checked for 
   conflicts.
 * field: The field definition (\Drupal\Core\Field\FieldDefinitionInterface
   object).
 * entity: The entity (\Drupal\Core\Entity\EntityInterface object) being
   validated.

Check \Drupal\Core\Database\Query\AlterableInterface for the methods to read
this data.


ISSUES
------

Issues should be reported at https://www.drupal.org/project/issues/date_recur_conflict.
