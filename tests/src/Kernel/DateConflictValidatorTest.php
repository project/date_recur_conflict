<?php

namespace Drupal\Tests\date_recur_conflict\Kernel;

use Drupal\date_recur_conflict\Plugin\Validation\Constraint\DateConflict;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;

/**
 * @coversDefaultClass \Drupal\date_recur_conflict\Plugin\Validation\Constraint\DateConflictValidator
 * @group date_recur_conflict
 */
class DateConflictValidatorTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'node',
    'field',
    'datetime',
    'datetime_range',
    'date_recur',
    'date_recur_conflict',
    'date_recur_conflict_test',
  ];

  /**
   * Tests validating the field when the cache table is empty.
   */
  public function testFirstEntry() {
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value);
    $errors = $node->validate();
    $this->assertEquals(0, $errors->count());
  }

  /**
   * Tests updating a previously saved entity without changing its date value.
   */
  public function testUpdateEntity() {
    // Saves the entity once.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value, TRUE);

    // Reassigns field value to force validation.
    $node->field_date->setValue($value);

    // Now validates.
    $errors = $node->validate();
    $this->assertEquals(0, $errors->count());
  }

  /**
   * Tests a conflict of an entry.
   */
  public function testLeftConflict() {
    // Saves the first entity.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value, TRUE);

    // Creates an entity whose date ends after the saved entity starts, but
    // starts before it.
    $other_value = $this->prepareRecurFieldValue('2020-01-01T03:00:00', '2020-01-01T04:30:00');
    $other_node = $this->createTestNode($other_value);

    $errors = $other_node->validate();
    $this->assertEquals(1, $errors->count());

    $constraint = $errors->get(0)->getConstraint();
    $this->assertInstanceOf(DateConflict::class, $constraint);
  }

  /**
   * Tests a conflict of an entry.
   */
  public function testRightConflict() {
    // Saves the first entity.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value, TRUE);

    // Creates an entity whose date starts before the saved entity ends, but
    // ends after it.
    $other_value = $this->prepareRecurFieldValue('2020-01-01T04:30:00', '2020-01-01T06:00:00');
    $other_node = $this->createTestNode($other_value);

    $errors = $other_node->validate();
    $this->assertEquals(1, $errors->count());

    $constraint = $errors->get(0)->getConstraint();
    $this->assertInstanceOf(DateConflict::class, $constraint);
  }

  /**
   * Tests a conflict of an entry.
   */
  public function testInnerConflict() {
    // Saves the first entity.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value, TRUE);

    // Creates an entity whose date starts after the saved entity starts, and
    // ends before it.
    $other_value = $this->prepareRecurFieldValue('2020-01-01T04:30:00', '2020-01-01T04:59:00');
    $other_node = $this->createTestNode($other_value);

    $errors = $other_node->validate();
    $this->assertEquals(1, $errors->count());

    $constraint = $errors->get(0)->getConstraint();
    $this->assertInstanceOf(DateConflict::class, $constraint);
  }

  /**
   * Tests a conflict of an entry.
   */
  public function testOuterConflict() {
    // Saves the first entity.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value, TRUE);

    // Creates an entity whose date starts before the saved entity starts, and
    // ends after it.
    $other_value = $this->prepareRecurFieldValue('2020-01-01T03:30:00', '2020-01-01T05:30:00');
    $other_node = $this->createTestNode($other_value);

    $errors = $other_node->validate();
    $this->assertEquals(1, $errors->count());

    $constraint = $errors->get(0)->getConstraint();
    $this->assertInstanceOf(DateConflict::class, $constraint);
  }

  /**
   * Tests a conflict of an entry.
   */
  public function testSingleOccurrenceConflict() {
    // Saves the first entity.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value, TRUE);

    // Creates an entity that conflicts in a single occurrence.
    $other_value = $this->prepareRecurFieldValue('2019-12-31T04:00:00', '2020-12-31T05:00:00', 'DAILY', 2, 1);
    $other_node = $this->createTestNode($other_value);

    $errors = $other_node->validate();
    $this->assertEquals(1, $errors->count());

    $constraint = $errors->get(0)->getConstraint();
    $this->assertInstanceOf(DateConflict::class, $constraint);
  }

  /**
   * Tests of an entry without conflict.
   */
  public function testEndsBeforeHour() {
    // Saves the first entity.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value, TRUE);

    $other_value = $this->prepareRecurFieldValue('2020-01-01T03:00:00', '2020-01-01T04:00:00');
    $other_node = $this->createTestNode($other_value);

    $errors = $other_node->validate();
    $this->assertEquals(0, $errors->count());
  }

  /**
   * Tests of an entry without conflict.
   */
  public function testStartsAfterHour() {
    // Saves the first entity.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value, TRUE);

    $other_value = $this->prepareRecurFieldValue('2020-01-01T05:00:00', '2020-01-01T06:00:00');
    $other_node = $this->createTestNode($other_value);

    $errors = $other_node->validate();
    $this->assertEquals(0, $errors->count());
  }

  /**
   * Tests of an entry without conflict.
   */
  public function testEndsBeforeDate() {
    // Saves the first entity.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value, TRUE);

    $other_value = $this->prepareRecurFieldValue('2019-12-25T04:00:00', '2019-12-25T05:00:00');
    $other_node = $this->createTestNode($other_value);

    $errors = $other_node->validate();
    $this->assertEquals(0, $errors->count());
  }

  /**
   * Tests of an entry without conflict.
   */
  public function testStartsAfterDate() {
    // Saves the first entity.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value, TRUE);

    $other_value = $this->prepareRecurFieldValue('2020-01-06T04:00:00', '2020-01-06T05:00:00');
    $other_node = $this->createTestNode($other_value);

    $errors = $other_node->validate();
    $this->assertEquals(0, $errors->count());
  }

  /**
   * Tests a conflict of a non recurring entry.
   */
  public function testLeftConflictNonRecur() {
    // Saves the first entity.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value, TRUE);

    $other_value = $this->prepareNonRecurFieldValue('2020-01-01T03:00:00', '2020-01-01T04:30:00');
    $other_node = $this->createTestNode($other_value);

    $errors = $other_node->validate();
    $this->assertEquals(1, $errors->count());

    $constraint = $errors->get(0)->getConstraint();
    $this->assertInstanceOf(DateConflict::class, $constraint);
  }

  /**
   * Tests when a conflict is added during update.
   */
  public function testConflictOnUpdate() {
    // Saves the first entity.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode($value, TRUE);

    // Creates a second node without conflict at first.
    $other_value = $this->prepareRecurFieldValue('2020-01-06T04:00:00', '2020-01-06T05:00:00');
    $other_node = $this->createTestNode($other_value, TRUE);

    // Updates the node introducing the conflict.
    $other_value = $this->prepareRecurFieldValue('2020-01-04T04:00:00', '2020-01-04T05:00:00');
    $other_node->field_date->setValue($other_value);

    $errors = $other_node->validate();
    $this->assertEquals(1, $errors->count());

    $constraint = $errors->get(0)->getConstraint();
    $this->assertInstanceOf(DateConflict::class, $constraint);
  }

  /**
   * Tests with disabled validation.
   */
  public function testConflictDisabledValidation() {
    // Saves the first entity.
    $value = $this->prepareRecurFieldValue();
    $node = $this->createTestNode();
    $node->field_date_conflict->setValue($value);
    $node->save();

    // Creates an entity whose date conflicts with previously saved entity.
    $other_node = $this->createTestNode();
    $other_node->field_date_conflict->setValue($value);

    $errors = $other_node->validate();
    $this->assertEquals(0, $errors->count());
  }

  /**
   * Creates a node of type 'test'.
   *
   * @param array $date_value
   *   Optional value to use for the recurring date field.
   * @param bool $save
   *   Whether to save the created node or not.
   *
   * @return \Drupal\node\Entity\NodeInterface
   *   The created node.
   */
  protected function createTestNode(array $date_value = NULL, $save = FALSE) {
    $node = Node::create([
      'type' => 'test',
      'title' => 'Test',
    ]);

    if (isset($date_value)) {
      $node->field_date->setValue($date_value);
    }

    if ($save) {
      $node->save();
    }

    return $node;
  }

  /**
   * Prepares field value with recurrence.
   *
   * @return array
   *   The value of a single delta.
   */
  protected function prepareRecurFieldValue($start_date = '2020-01-01T04:00:00',
                                            $end_date = '2020-01-01T05:00:00',
                                            $freq = 'DAILY',
                                            $interval = 1,
                                            $count = 5,
                                            $infinite = FALSE) {
    return [
      'value' => $start_date,
      'end_value' => $end_date,
      'rrule' => "FREQ=$freq;INTERVAL=$interval;COUNT=$count",
      'timezone' => 'America/Sao_Paulo',
      'infinite' => $infinite,
    ];
  }

  /**
   * Prepares field value without repetitions.
   *
   * @return array
   *   The value of a single delta.
   */
  protected function prepareNonRecurFieldValue($start_date = '2020-01-01T04:00:00',
                                               $end_date = '2020-01-01T05:00:00') {
    return [
      'value' => $start_date,
      'end_value' => $end_date,
      'rrule' => '',
      'timezone' => 'America/Sao_Paulo',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installConfig(['date_recur_conflict_test', 'system']);
  }

}
