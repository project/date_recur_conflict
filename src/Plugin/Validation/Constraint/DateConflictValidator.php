<?php

namespace Drupal\date_recur_conflict\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationManager;
use Drupal\date_recur\DateRange;
use Drupal\date_recur\DateRecurHelper;
use Drupal\date_recur\DateRecurOccurrences;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the DateRecurConflict constraint.
 */
class DateConflictValidator extends ConstraintValidator implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $db;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * String translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;

  /**
   * Maximum end date stored in the cache table.
   *
   * @var \DateTimeInterface
   */
  private $maxEndDate;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('database'), $container->get('date.formatter'), $container->get('string_translation'));
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $db
   *   Database connection.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter service.
   * @param \Drupal\Core\StringTranslation\TranslationManager $translation
   *   String translation service.
   */
  public function __construct(Connection $db, DateFormatterInterface $date_formatter, TranslationManager $translation) {
    $this->db = $db;
    $this->dateFormatter = $date_formatter;
    $this->stringTranslation = $translation;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    $field_definition = $items->getFieldDefinition();
    $entity = $items->getEntity();

    // Checks conflicts for each item in the list.
    $values = $items->getValue();
    if (!empty($values)) {
      // Iterates over field deltas.
      foreach ($values as $val) {
        if (isset($val['value']) && isset($val['end_value'])) {
          // Checks conflicts for each occurrence.
          $interval = $this->getOccurrencesInterval($val, $field_definition);
          $occurrences = $this->getOccurrences($val, $interval);
          foreach ($occurrences as $occur) {
            if ($this->checkConflictingOccurrence($occur, $field_definition, $entity)) {
              // Conflicting occurrence found.
              $msg = $this->formatErrorMessage($constraint, $occur);
              $this->context->addViolation($msg);
              return;
            }
          }
        }
      }
    }
  }

  /**
   * Gets date occurrences for the field value.
   *
   * @param array $field_value
   *   The value of a specific delta of the field. It must contain the "value",
   *   "end_value".
   * @param \Drupal\date_recur\DateRange $interval
   *   The period for which the occurrences should be generated.
   *
   * @return \Drupal\date_recur\DateRange[]
   *   Occurrences of the reccurring (or not) date.
   */
  protected function getOccurrences(array $field_value, DateRange $interval) {
    $occurrences = [];

    // The date strings have to be converted from UTC to the local timezone.
    $utc_tz = new \DateTimeZone('UTC');
    $local_tz = new \DateTimeZone($field_value['timezone']);
    $start_date = (new \DateTime($field_value['value'], $utc_tz))
      ->setTimezone($local_tz);
    $end_date = (new \DateTime($field_value['end_value'], $utc_tz))
      ->setTimezone($local_tz);

    if (empty($field_value['rrule'])) {
      // Non recurring date.
      $occurrences[] = new DateRange($start_date, $end_date);
    }
    else {
      // Generates occurrences in the specified interval.
      $rrule = $field_value['rrule'];
      $helper = DateRecurHelper::create($rrule, $start_date, $end_date);
      $occurrences = $helper->getOccurrences($interval->getStart(), $interval->getEnd());
    }

    return $occurrences;
  }

  /**
   * Gets the interval for which occurrences should be generated for validation.
   *
   * @param array $field_value
   *   The value of a specific delta of the field. It must contain at least the
   *   "value" key.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return \Drupal\date_recur\DateRange
   *   The occurrences period.
   */
  protected function getOccurrencesInterval(array $field_value, FieldDefinitionInterface $field_definition) {
    $field_name = $field_definition->getName();

    // Gets the occurrences table name.
    $field_storage = $field_definition->getFieldStorageDefinition();
    $table_name = DateRecurOccurrences::getOccurrenceCacheStorageTableName($field_storage);

    // Dates are stored in UTC timezone.
    $utc_tz = new \DateTimeZone('UTC');

    // The generated occurrences start with the first date filled.
    $interval_start = (new \DateTime($field_value['value'], $utc_tz));

    // Checks if the latest end date is cached.
    if (!isset($this->maxEndDate)) {
      // Builds a query to get the maximum end date from the cache table.
      $result = $this->db->query("SELECT MAX({$field_name}_end_value) FROM {{$table_name}}");
      if ($end_str = $result->fetchField()) {
        // Caches the result to avoid repeating the query for each delta.
        $this->maxEndDate = new \DateTime($end_str, $utc_tz);
      }
      else {
        // There are no occurrences stored in the database.
        $this->maxEndDate = FALSE;
      }
    }

    if ($this->maxEndDate !== FALSE && $this->maxEndDate > $interval_start) {
      // The occurrences will be generated until the latest cached occurrence.
      $interval_end = $this->maxEndDate;
    }
    else {
      // There are no occurrences finishing after the start date in the
      // database. So the interval can be squeezed as there is no need to
      // generate any occurrences.
      $interval_end = $interval_start;
    }

    // Returns a DateRange object.
    $interval = new DateRange($interval_start, $interval_end);
    return $interval;
  }

  /**
   * Checks if an occurrence conflicts with values stored in the database.
   *
   * @param \Drupal\date_recur\DateRange $occurrence
   *   The date occurrence whose conflicts will be checked.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition of the field whose occurrence will be checked.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity whose field is validated.
   *
   * @return bool
   *   Returns TRUE if there are conflicting occurrences. Otherwise, returns
   *   FALSE.
   */
  protected function checkConflictingOccurrence(DateRange $occurrence,
                                                FieldDefinitionInterface $field_definition,
                                                EntityInterface $entity) {
    $field_name = $field_definition->getName();
    $entity_type_id = $field_definition->getTargetEntityTypeId();
    $bundle = $field_definition->getTargetBundle();

    // Gets the occurrences table name.
    $field_storage = $field_definition->getFieldStorageDefinition();
    $table_name = DateRecurOccurrences::getOccurrenceCacheStorageTableName($field_storage);

    // Converts dates timezone because they are stored in UTC.
    $utc_tz = new \DateTimeZone('UTC');
    $start_date = (clone $occurrence->getStart())
      ->setTimezone($utc_tz);
    $end_date = (clone $occurrence->getEnd())
      ->setTimezone($utc_tz);

    // Converts dates to string for comparison with stored data.
    $start_str = $start_date->format(DateRecurItem::DATETIME_STORAGE_FORMAT);
    $end_str = $end_date->format(DateRecurItem::DATETIME_STORAGE_FORMAT);

    // Builds the query.
    $query = $this->db->select($table_name, 'occ')
      ->fields('occ', ['entity_id'])
      ->condition("occ.{$field_name}_value", $end_str, '<')
      ->condition("occ.{$field_name}_end_value", $start_str, '>')
      ->range(0, 1);

    // If the entity has already been saved before, it should not be compared
    // with itself.
    if ($entity_id = $entity->id()) {
      $query->condition('occ.entity_id', $entity_id, '<>');
    }

    // Adds tags to allow the query to be easily altered.
    $query
      ->addTag("date_recur_conflict__{$entity_type_id}__{$bundle}__{$field_name}")
      ->addTag("date_recur_conflict__{$entity_type_id}__{$bundle}")
      ->addTag("date_recur_conflict__{$entity_type_id}__{$field_name}")
      ->addTag("date_recur_conflict__{$entity_type_id}")
      ->addTag("date_recur_conflict__{$field_name}");

    // Includes metadata that might be used by alter hook implementations.
    $query
      ->addMetaData('occurrence', $occurrence)
      ->addMetaData('field', $field_definition)
      ->addMetaData('entity', $entity);

    // Executes the query to check if there any conflicts.
    $result = $query->execute()->fetchAll();
    return !empty($result);
  }

  /**
   * Formats a validation error message.
   *
   * @param \Symfony\Component\Validator\Constraint $constraint
   *   The constraint for validation.
   * @param \Drupal\date_recur\DateRange $occurrence
   *   The date occurrence that conflicted.
   *
   * @return string
   *   The error message.
   */
  protected function formatErrorMessage(Constraint $constraint, DateRange $occurrence) {
    $start_time = $occurrence->getStart()->getTimestamp();
    $end_time = $occurrence->getEnd()->getTimestamp();

    // Formats the message.
    $start_str = $this->dateFormatter->format($start_time, 'short');
    $end_str = $this->dateFormatter->format($end_time, 'short');
    $message = $this->t($constraint->errorMsg, [
      '@start' => $start_str,
      '@end' =>$end_str,
    ]);
    return $message;
  }

}
