<?php

namespace Drupal\date_recur_conflict\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if there are any conflicts in date occurrences.
 *
 * @Constraint(
 *   id = "DateRecurConflict",
 *   label = @Translation("Date Recur Conflict", context="Validation"),
 *   type = "date_recur"
 * )
 */
class DateConflict extends Constraint {

  /**
   * Error shown when conflicting dates are found.
   *
   * @var string
   */
  public $errorMsg = 'There are one or more conflicting occurrences in the period from @start to @end.';

}
